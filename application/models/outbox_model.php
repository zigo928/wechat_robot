<?php

/*
 * Inbox Model
 */

class Outbox_model extends CI_Model {

    private $tableName = 'u_m_outbox';

    public function __construct() {
        parent::__construct();
    }

    /*
     * save msg
     * @param   string
     * @param   string
     */

    public function saveMsg($toUser, $XMLData) {
        $set = array(
            'to_user' => strval($toUser),
            'xml_data' => $XMLData,
            'create_time' => time()
        );

        $this->db->insert($this->tableName, $set);

        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

}