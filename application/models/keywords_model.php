<?php

class Keywords_model extends CI_Model {

    private $tableName = 'u_m_keywords';

    public function __construct() {
        parent::__construct();
    }

    public function getKeywordByActionKey($actionKey) {
        $where['action_key'] = $actionKey;

        $result = $this->db->get_where($this->tableName, $where, 1);

        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function getKeyword($keyword, $msgType_id = 0, $is_byId = 0, $likeMode = 0) {
        if ($msgType_id > 0) {
            $where['type'] = $msgType_id;
        }

        if (1 == $is_byId) {
            $where['id'] = intval($keyword);
        } else {
            if (1 == $likeMode) {
                $this->db->like('keyword', $keyword);
            } else {
                $where['keyword'] = $keyword;
            }
        }

        $result = $this->db->get_where($this->tableName, $where, 1);
//        return $this->db->last_query();

        if ($result->num_rows() > 0) {
            $return = $result->row();
            $result->free_result();
            return $return;
        }

        return FALSE;
    }

}