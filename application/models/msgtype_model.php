<?php

class Msgtype_model extends CI_Model {

    private $tableName = 'u_c_msgtype';

    public function __construct() {
        parent::__construct();
    }

    public function getMsgType($type, $is_byId = 0) {
        if (1 == $is_byId) {
            $where['classid'] = $type;
        } else {
            $where['type_name'] = $type;
        }

        $result = $this->db->get_where($this->tableName, $where, 1);

        if ($result->num_rows() > 0) {
            return $result->row();
        }

        return FALSE;
    }

}
