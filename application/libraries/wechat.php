<?php

/*
 * wechat robot
 * @author  terminus
 * @email   terminus.ye@gmail.com
 * @date    2013-03-23
 */

class wechat {

    private $_CI;
    //TOKEN
    private $token;
    //cmd_expire_time
    private $cmd_expire_time = 300;
    //tpls
    private $textTpl;
    private $newsTpl;
    private $musicTpl;
    //user id
    private $fromUser; //yourself
    private $toUser; //user who sent msg to u

    public function __construct() {
        $this->_CI = &get_instance();
        $this->_CI->load->config('wechat', TRUE);
        //load TOKEN
        $this->token = $this->_CI->config->item('token', 'wechat');
        //load cmd_expire_time
        $this->cmd_expire_time = $this->_CI->config->item('cmd_expire_time', 'wechat');
        //load tpls
        $this->textTpl = $this->_CI->config->item('textTpl', 'wechat');
        $this->newsTpl = $this->_CI->config->item('newsTpl', 'wechat');
        $this->musicTpl = $this->_CI->config->item('musicTpl', 'wechat');
    }

    /*
     * valid
     */

    public function valid($data) {
        $echoStr = $data['echostr'];
        $signature = $data['signature'];
        $timestamp = $data['timestamp'];
        $nonce = $data['nonce'];

        if ($this->_checkSignature($signature, $timestamp, $nonce)) {
            echo $echoStr;
            exit;
        }
    }

    /*
     * development check function
     */

    private function _checkSignature($signature, $timestamp, $nonce) {
        $tmpArr = array($this->token, $timestamp, $nonce);
        sort($tmpArr);
        $tmpStr = implode($tmpArr);

        if (sha1($tmpStr) == $signature) {
            return true;
        }
        return false;
    }

    public function test() {
        
    }

    /*
     * check user ? blacklist
     */

    private function _isbaned($opn_id) {
        $this->_CI->load->model('blacklist_model', 'blacklist');
        if (FALSE === $this->_CI->blacklist->getList($opn_id)) {
            return FALSE;
        }
        return TRUE;
    }

    /*
     * select slove way by user msg type
     */

    public function resloveMsg($serverMsg) {
        $postObj = simplexml_load_string($serverMsg, 'SimpleXMLElement', LIBXML_NOCDATA);
        $this->toUser = strval($postObj->FromUserName);
        $this->fromUser = strval($postObj->ToUserName);
        $msgType = $postObj->MsgType;

        //save msg to db
        $this->_saveInMsg($postObj->FromUserName, $postObj);

        //blacklist
        if (TRUE === $this->_isbaned($this->toUser)) {
            exit('you are be banned.');
        }
        
        //get msg type id
        $this->_CI->load->model('msgtype_model', 'msgtype');
        $msgTypeId = @$this->_CI->msgtype->getMsgType(strval($postObj->MsgType))->classid;
        if (!$msgTypeId) {
            $this->_sendHelpMsg();
            return 0;
        }

        //select response method
        switch ($msgType) {
            case 'text':
                $this->_MsgText($postObj, $msgTypeId);
                break;
            case 'image':
                $this->_MsgImage($postObj, $msgTypeId);
                break;
            case 'location':
                $this->_MsgLocation($postObj, $msgTypeId);
                break;
            case 'link':
                $this->_MsgLink($postObj, $msgTypeId);
                break;
            case 'event':
                $this->_MsgEvent($postObj, $msgTypeId);
                break;
            case 'voice':
                $this->_MsgVoice($postObj, $msgTypeId);
                break;
            default:
                $this->_sendHelpMsg();
                break;
        }
    }

    /*
     * save in msg to db
     */

    private function _saveInMsg($fromUser, $postObj) {
        //convert
        $jsonData = json_encode($postObj);
        //tmp file
        write_file('./lastInMsg.txt', $jsonData);

        $this->_CI->load->model('inbox_model', 'inbox');
        //save to db
        $this->_CI->inbox->saveMsg($fromUser, $jsonData);
    }

    /*
     * save out msg to db
     */

    private function _saveOutMsg($toUser, $resultXML) {
        //tmp file
        write_file('./lastOutMsg.txt', $resultXML);

        $this->_CI->load->model('outbox_model', 'outbox');
        //save to db
        $this->_CI->outbox->saveMsg($toUser, $resultXML);
    }

    /*
     * send help msg
     */

    private function _sendHelpMsg() {
        $this->_responseMsg($this->_getStoredMsg(0));
    }

    /*
     * slove voice msg
     */

    private function _MsgVoice($postObj, $msgTypeId) {
        $this->_sendHelpMsg();
    }

    /*
     * 处理命令
     */

    private function _resloveCmd($user_input) {
        $this->_CI->load->model('cmd_response_model', 'cmd_response');

        $user_input = trim(strval($user_input));
        $matches = '';
        //获取用户tmp
        $tmp = $this->_getTmp($this->toUser);
        //获取命令代码
        $reg = '/^([a-zA-Z]{2}[1-9]{0,2})#/';
        if (preg_match($reg, $user_input, $matches) > 0) {
            $action_key = strtolower($matches[1]);
            //查找命令解决方案
            $cmd_re = $this->_CI->cmd_response->getResponse($action_key);

            if (FALSE == $cmd_re) {
                //对应命令不存在
                $this->_responseMsg($this->_CI->cmd_response->getResponse('err_unknownkey'));
                return 0;
            }

            if (1 === intval($cmd_re->is_expire)) {
                //命令需检查过期
                if (time() - $tmp->update_time > $this->cmd_expire_time) {
                    //现在时间-上次命令时间>设定过期时间
                    //命令过期
                    $this->_responseMsg($this->_CI->cmd_response->getResponse('err_expired'));
                    return 0;
                }
            }

            if (intval($cmd_re->pkey) > 0) {
                //命令需父行为
                //当前命令pkey
                $c_pkey = strval($this->_CI->cmd_response->getResponse(intval($cmd_re->pkey), 1)->for_action_key);
                if (strval($tmp->action_key) !== $c_pkey) {//当前命令pkey与tmpkey比较
                    //命令父行为不符
                    $this->_responseMsg($this->_CI->cmd_response->getResponse('err_invalidpkey'));
                    return 0;
                }
            }

            if (1 === intval($tmp->need_confirm)) {
                //上次命令需确认
                $dreg = '/' . $cmd_re->dreg . '/';
                if (preg_match($dreg, $user_input, $matches) > 0) {
                    //命令对应数据
                    $cdata = $matches[1];
                    switch ($cdata) {
                        case '1':
                            //确认前步操作,根据前步骤确定逻辑走向
                            //TODO
                            $this->_clearTmp($this->toUser);
                            return 0;
                            break;
                        default:
                            $this->_clearTmp($this->toUser);
                            $this->_responseMsg($this->_CI->cmd_response->getResponse('cancel'));
                            return 0;
                            break;
                    }
                } else {
                    //无法按照既定格式获取到命令数据
                    $this->_responseMsg($this->_CI->cmd_response->getResponse('err_invalidkey'));
                    return 0;
                }
            }

            //获取命令数据
            $dreg = $cmd_re->dreg;
            if ($dreg) {
                $dreg = '/' . $dreg . '/';
                //需要获取数据
                if (preg_match($dreg, $user_input, $matches) > 0) {
                    $cdata = $matches[1];
                    //获取已保存的tmp json
                    $jsondata = json_decode($tmp->json_data);
                    if (NULL == $jsondata) {
                        //若json数据不存在,则新建对象
                        $jsondata = new stdClass();
                    }
                    //保存格式为 命令->命令数据
                    $jsondata->$action_key = $cdata;
                    $jsondata = json_encode($jsondata);
                    //存入tmp
                    if (!$this->_saveTmp($this->toUser, $jsondata, $action_key, $cmd_re->is_need_confirm)) {
                        //保存数据出错
                        $this->_responseMsg($this->_CI->cmd_response->getResponse('err_error'));
                    }
                    //输出提示文字
                    $this->_responseMsg($cmd_re);
                    return 0;
                } else {
                    //无法按照既定格式获取到命令数据
                    $this->_responseMsg($this->_CI->cmd_response->getResponse('err_invalidcdata'));
                    return 0;
                }
            } else {
                //输出提示文字
                $this->_responseMsg($cmd_re);
                return 0;
            }
        } else {
            //无法按既定格式获取到命令代码,返回无效命令消息
            $this->_responseMsg($this->_CI->cmd_response->getResponse('err_unknownkey'));
            return 0;
        }
    }

    /*
     * 获取tmp
     */

    private function _getTmp($user_id) {
        $this->_CI->load->model('tmp_model', 'tmp');
        $tmp = $this->_CI->tmp->getTmp($user_id);
        if (FALSE == $tmp) {
            return $this->_CI->tmp->addTmp($user_id);
        }
        return $tmp;
    }

    /*
     * 清空tmp
     */

    private function _clearTmp($user_id) {
        $this->_saveTmp($user_id, '', NULL, 0);
    }

    /*
     * 保存tmp
     */

    private function _saveTmp($user_id, $jsonData, $action_key = '', $need_confirm = 0) {
        $this->_CI->load->model('tmp_model', 'tmp');
        return $this->_CI->tmp->setTmp($user_id, $action_key, $need_confirm, $jsonData);
    }

    /*
     * slove text msg
     */

    private function _MsgText($postObj, $msgTypeId) {
        if (FALSE !== strpos(strval($postObj->Content), '#')) {
            //reslove command
            $this->_resloveCmd($postObj->Content);
        } else {
            //common
            $this->_CI->load->model('keywords_model', 'keywords');

            $keyword_id = @$this->_CI->keywords->getKeyword($postObj->Content, $msgTypeId, 0, 1)->id;
            if (!$keyword_id) {
                $this->_sendHelpMsg();
                return 0;
            }

            $response = $this->_getStoredMsg($keyword_id);

            if (!$response) {
                $this->_sendHelpMsg();
                return 0;
            }

            $this->_responseMsg($response);
        }
    }

    /*
     * slove image msg
     */

    private function _MsgImage($postObj, $msgTypeId) {
        $this->_sendHelpMsg();
    }

    /*
     * slove location msg
     */

    private function _MsgLocation($postObj, $msgTypeId) {
        //test data
        $msg = new stdClass();
        $msg->msg_type = 1;
        $msg->content = '哔哔哔，核弹瞄准...发射！';
        $this->_responseMsg($msg);
    }

    /*
     * slove link msg
     */

    private function _MsgLink($postObj, $msgTypeId) {
        $this->_sendHelpMsg();
    }

    /*
     * slove event msg
     */

    private function _MsgEvent($postObj, $msgTypeId) {
        $this->_CI->load->model('keywords_model', 'keywords');

        $keyword_id = @$this->_CI->keywords->getKeyword($postObj->Event, $msgTypeId, 0, 1)->id;
        if (!$keyword_id) {
            $this->_sendHelpMsg();
            return 0;
        }

        $response = $this->_getStoredMsg($keyword_id);

        if (!$response) {
            $this->_sendHelpMsg();
            return 0;
        }

        $this->_responseMsg($response);
    }

    /*
     * get stored msg for keyword
     */

    private function _getStoredMsg($for_keyword, $is_forcmd = 0) {
        if (1 == $is_forcmd) {
            $this->_CI->load->model('cmd_response_model', 'cmd_response');

            return $this->_CI->cmd_response->getResponse($for_keyword);
        } else {
            $this->_CI->load->model('response_model', 'response');

            $responses = $this->_CI->response->getResponse($for_keyword);

            if ($responses) {
                return $responses[array_rand($responses)];
            }
            return FALSE;
        }
    }

    /*
     * response msg
     */

    private function _responseMsg($msgData) {
        if (!is_object($msgData)) {
            exit;
        }

        $this->_CI->load->model('msgtype_model', 'msgtype');
        //select msg type
        $msgData->msgType = $this->_CI->msgtype->getMsgType($msgData->msg_type, 1)->type_name;
        switch ($msgData->msgType) {
            case 'text':
                /*
                 * options:
                 * xml,
                 * sender,
                 * consignee,
                 * send time,
                 * message type[text],
                 * content
                 */
                $resultStr = sprintf($this->textTpl, $this->toUser, $this->fromUser, time(), $msgData->msgType, $msgData->content);
                break;
            case 'news':
                /*
                 * options:
                 * xml,
                 * sender,
                 * consignee,
                 * send time,
                 * message type[news],
                 * news title,
                 * news description,
                 * news picture,
                 * news url
                 */
                $resultStr = sprintf($this->newsTpl, $this->toUser, $this->fromUser, time(), $msgData->msgType, $msgData->title, $msgData->description, $msgData->pic_url, $msgData->link_url);
                break;
            case 'music':
                /*
                 * options:
                 * xml,
                 * sender,
                 * consignee,
                 * send time,
                 * message type[music],
                 * music title,
                 * music description,
                 * music url,
                 * HQ music url[wifi mode will play this one]
                 */
                $resultStr = sprintf($this->musicTpl, $this->toUser, $this->fromUser, time(), $msgData->msgType, $msgData->title, $msgData->description, $msgData->music_url, $msgData->hqmusic_url);
                break;
            default :
                //wrong type,exit
                $resultStr = 'wrong type ' . $msgData->msgType;
                break;
        }
        //delete space char
        $resultXML = preg_replace('/[\r|\t]/', '', $resultStr);
        //save out msg
        $this->_saveOutMsg($this->toUser, $resultXML);
        //output msg
        echo $resultXML;
    }

}