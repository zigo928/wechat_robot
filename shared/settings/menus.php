<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['menus']=array (
  0 => 
  array (
    'menu_id' => '1',
    'class_name' => 'system',
    'method_name' => 'home',
    'menu_name' => '系统',
    'sub_menus' => 
    array (
      0 => 
      array (
        'menu_id' => '2',
        'class_name' => 'system',
        'method_name' => 'home',
        'menu_name' => '后台首页',
        'sub_menus' => 
        array (
          0 => 
          array (
            'menu_id' => '3',
            'class_name' => 'system',
            'method_name' => 'home',
            'menu_name' => '后台首页',
          ),
        ),
      ),
      1 => 
      array (
        'menu_id' => '4',
        'class_name' => 'setting',
        'method_name' => 'site',
        'menu_name' => '系统设置',
        'sub_menus' => 
        array (
          0 => 
          array (
            'menu_id' => '5',
            'class_name' => 'setting',
            'method_name' => 'site',
            'menu_name' => '站点设置',
          ),
          1 => 
          array (
            'menu_id' => '6',
            'class_name' => 'setting',
            'method_name' => 'backend',
            'menu_name' => '后台设置',
          ),
          2 => 
          array (
            'menu_id' => '7',
            'class_name' => 'system',
            'method_name' => 'password',
            'menu_name' => '修改密码',
          ),
          3 => 
          array (
            'menu_id' => '8',
            'class_name' => 'system',
            'method_name' => 'cache',
            'menu_name' => '更新缓存',
          ),
        ),
      ),
      2 => 
      array (
        'menu_id' => '9',
        'class_name' => 'model',
        'method_name' => 'view',
        'menu_name' => '模型管理',
        'sub_menus' => 
        array (
          0 => 
          array (
            'menu_id' => '10',
            'class_name' => 'model',
            'method_name' => 'view',
            'menu_name' => '内容模型管理',
          ),
          1 => 
          array (
            'menu_id' => '11',
            'class_name' => 'category',
            'method_name' => 'view',
            'menu_name' => '分类模型管理',
          ),
        ),
      ),
      3 => 
      array (
        'menu_id' => '12',
        'class_name' => 'plugin',
        'method_name' => 'view',
        'menu_name' => '插件管理',
        'sub_menus' => 
        array (
          0 => 
          array (
            'menu_id' => '13',
            'class_name' => 'plugin',
            'method_name' => 'view',
            'menu_name' => '插件管理',
          ),
        ),
      ),
      4 => 
      array (
        'menu_id' => '14',
        'class_name' => 'role',
        'method_name' => 'view',
        'menu_name' => '权限管理',
        'sub_menus' => 
        array (
          0 => 
          array (
            'menu_id' => '15',
            'class_name' => 'role',
            'method_name' => 'view',
            'menu_name' => '用户组管理',
          ),
          1 => 
          array (
            'menu_id' => '16',
            'class_name' => 'user',
            'method_name' => 'view',
            'menu_name' => '用户管理',
          ),
        ),
      ),
    ),
  ),
  1 => 
  array (
    'menu_id' => '17',
    'class_name' => 'content',
    'method_name' => 'view',
    'menu_name' => '内容管理',
    'sub_menus' => 
    array (
      0 => 
      array (
        'menu_id' => '18',
        'class_name' => 'content',
        'method_name' => 'view',
        'menu_name' => '内容管理',
        'sub_menus' => 
        array (
          0 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'inbox',
            'menu_name' => '收信箱',
          ),
          1 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'outbox',
            'menu_name' => '发件箱',
          ),
          2 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'keywords',
            'menu_name' => '关键字',
          ),
          3 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'tmp',
            'menu_name' => '临时数据',
          ),
          4 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'response',
            'menu_name' => '预定义回复',
          ),
          5 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'cmd_response',
            'menu_name' => '命令预定义回复',
          ),
          6 => 
          array (
            'class_name' => 'content',
            'method_name' => 'view',
            'extra' => 'blacklist',
            'menu_name' => '黑名单',
          ),
        ),
      ),
      1 => 
      array (
        'menu_id' => '19',
        'class_name' => 'category_content',
        'method_name' => 'view',
        'menu_name' => '分类管理',
        'sub_menus' => 
        array (
          0 => 
          array (
            'class_name' => 'category_content',
            'method_name' => 'view',
            'extra' => 'msgtype',
            'menu_name' => '消息类型',
          ),
        ),
      ),
    ),
  ),
  2 => 
  array (
    'menu_id' => '20',
    'class_name' => 'module',
    'method_name' => 'run',
    'menu_name' => '工具',
    'sub_menus' => 
    array (
    ),
  ),
);