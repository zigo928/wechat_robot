<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['category']['msgtype']=array (
  1 => 
  array (
    'classid' => '1',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'text',
    'description' => '文本',
    'children' => '0',
    'deep' => 0,
  ),
  2 => 
  array (
    'classid' => '2',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'image',
    'description' => '图片',
    'children' => '0',
    'deep' => 0,
  ),
  3 => 
  array (
    'classid' => '3',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'event',
    'description' => '事件推送',
    'children' => '0',
    'deep' => 0,
  ),
  4 => 
  array (
    'classid' => '4',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'location',
    'description' => '地理位置',
    'children' => '0',
    'deep' => 0,
  ),
  5 => 
  array (
    'classid' => '5',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'link',
    'description' => '链接',
    'children' => '0',
    'deep' => 0,
  ),
  6 => 
  array (
    'classid' => '6',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'news',
    'description' => '图文超链接',
    'children' => '0',
    'deep' => 0,
  ),
  7 => 
  array (
    'classid' => '7',
    'parentid' => '0',
    'level' => '1',
    'path' => '',
    'type_name' => 'voice',
    'description' => '语音',
    'children' => '0',
    'deep' => 0,
  ),
);