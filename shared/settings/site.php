<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting=array (
  'site_name' => 'wechat_robot',
  'site_domain' => '',
  'site_logo' => 'images/logo.gif',
  'site_icp' => '',
  'site_terms' => '',
  'site_stats' => '',
  'site_footer' => '',
  'site_status' => '1',
  'site_close_reason' => '网站维护升级中......',
  'site_keyword' => '',
  'site_description' => '',
  'site_theme' => 'default',
  'attachment_url' => 'http://www.tt.com/weixin_robot/attachments',
  'attachment_dir' => 'attachments',
  'attachment_type' => '*.jpg;*.gif;*.png;*.mp3;*.wav',
  'attachment_maxupload' => '2097152',
);